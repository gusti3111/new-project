import { useState } from 'react'
import './App.css';

function App() {
  const [color, setColor] = useState("")
  return (
    <div className="App">
      <div style={{ backgroundColor: color !== 0 ? "red" : "yellow" }}>
        <h3>{color}</h3>
      </div>

    </div>
  );
}

export default App;
